 
 module dosiesta

      USE precision
      USE spher_harm
      USE interpola
      IMPLICIT NONE

      CONTAINS
      subroutine  siespart(deg,lmax,factorx,factory,factorz,choosedeg,chooseae,&
     &    choosatom,sysname,integral,potential,nspin)
 
      INTEGER,PARAMETER::natmax = 1000
      INTEGER   ::ind2,idist,indexv,ind1,ind3,isp,ix,iy, &
     &            iz,i,ip,natoms,np,mesh(3),id(3),iix,   &
     &            iiy,iiz, ii, length, lb,iax,iay,iaz,   &
     &            NewMesh(3),nspp,nskip,ierr,nmax=0,ios1,&
     &            shift,iv
      integer    :: mspx,mspy,mspz
      INTEGER,intent(in)     ::deg,lmax,nspin
      INTEGER,intent(in)     ::choosatom,factorx,factory,factorz
      INTEGER,PARAMETER                :: max_l = 5
      integer, parameter               :: max_ilm = (max_l+1)*(max_l+1)
    
      CHARACTER   :: sysname*70, programa*75,fnamein*75, fnameout(2)*75,&
     &               fnamexyz*75, paste*74, task*5
      CHARACTER(LEN=10),intent(in)   ::choosedeg,chooseae
      CHARACTER(LEN=3)               ::label(natmax)
      REAL(dp)                ::rcut,dist,fmin,fmax,a,b,c,dvol
      real, allocatable   ::POT(:,:,:,:)
      real,allocatable    ::RHO(:,:)
      REAL(dp)            :: cell(3,3),&
      &                      dr(3), rpoint(3),rrel(3),ratt(3),xat(natmax,3), &
      &                      rat(3),cellm(3),celang(3),volume, drmax
      real(dp)            :: ratfrac(3),rpoint0(3)
      real(dp),allocatable       :: ao(:),af(:),y2(:)
      real(dp)                   :: y1,yn,orbr,val,startx,starty,startz,np2
      real(dp)               ::grly(3,max_ilm),allang(max_ilm)
      real(dp)               ::orbang(max_ilm)
      real(dp), intent(out)  ::integral,potential
      real(dp), parameter    :: tiny4=1.e-4_dp
      real(dp), parameter         :: tiny8=1.e-8_dp
      REAL(dp), parameter  :: PI=3.14159265358979323846264338327950288419716939937510_dp
      REAL(dp), parameter  :: Ang    = 1._dp / 0.529177_dp
      REAL(dp), external   :: volcel
      external  paste, lb
!------------------------
!Read the AE wavefuncions                                                     
!-------------------------
        ios1=0
        nmax=0
        open(unit=19,file=chooseae,status='old',action='read',iostat=ios1)
        DO
         read(19,*,iostat=ios1)
         IF (ios1<0) EXIT
         nmax=nmax+1
        ENDDO
        rewind(19)
        allocate(ao(nmax),af(nmax),y2(nmax))
        do i=1,nmax
         read(19,*)ao(i),af(i)
        enddo
        do i = nmax,1,-1
          if( af(i) >= 0.000000001) then
          rcut=ao(i)
          exit
          end if
        enddo
        close(19)


! calls a subroutine that computes the derivate of the first and last points necessary to use with 
! the cubic spline subroutine
             call derivative(nmax,ao,af,y1,yn)
             call spline(ao,af,nmax,y1,yn,y2)

!-------------------------
!Reads the Total Potential
!-------------------------
      fnamein = paste(sysname,'.VT')

      length = lb(fnamein)

             write(11,*)
             write(11,*) 'Reading grid data from file ',fnamein(1:length)


     open( unit=1, file=fnamein, form="unformatted", status='old' )

     read(1) cell
     write(11,*)
     write(11,*) 'Cell vectors[Bohr]:'
     write(11,*)
     write(11,*) cell(1,1),cell(2,1),cell(3,1)
     write(11,*) cell(1,2),cell(2,2),cell(3,2)
     write(11,*) cell(1,3),cell(2,3),cell(3,3)
     read(1) mesh, nspp

     write(11,*)
     write(11,*) 'Grid mesh: ',mesh(1),'x',mesh(2),'x',mesh(3)
     write(11,*)
     write(11,*) 'nspin = ',nspp
     write(11,*)

     NewMesh(1)=factorx*mesh(1)
     NewMesh(2)=factory*mesh(2)
     NewMesh(3)=factorz*mesh(3)

!cell vector modules

      do iv = 1,3
        cellm(iv) = dot_product(cell(:,iv),cell(:,iv))
        cellm(iv) = sqrt(cellm(iv))
      enddo

      celang(1) = dot_product(cell(:,1),cell(:,2))
      celang(1) = acos(celang(1)/(cellm(1)*cellm(2)))*180._dp/pi
      celang(2) = dot_product(cell(:,1),cell(:,3))
      celang(2) = acos(celang(2)/(cellm(1)*cellm(3)))*180._dp/pi
      celang(3) = dot_product(cell(:,2),cell(:,3))
      celang(3) = acos(celang(3)/(cellm(2)*cellm(3)))*180._dp/pi

      write(11,'(/,a,3f12.6)')                    &
     & 'Cell vector modules (Ang)   :', &
     &          (cellm(iv)/Ang,iv=1,3)
      write(11,'(a,3f12.4)')                      &
     & 'Cell angles (23,13,12) (deg):', &
     &          (celang(iv),iv=3,1,-1)


celdaL:    do ix=1,3
           dr(ix)=cellm(ix)/NewMesh(ix)
           enddo celdaL
           write(11,*)
           write(11,*)
!           write(11,*) 'New Volume element='
!           write(11,*) dr(1),'x',dr(2),'x',dr(3)
!           np2 =real(NewMesh(1))*real(NewMesh(2))*real(NewMesh(3))
           write(11,*)
           write(11,*) 'New Grid mesh: ',Newmesh(1),'x',Newmesh(2),'x',Newmesh(3)
           write(11,*)
!           write(11,*)'New np=',np2


!--------------------------------------
!Reads the coordinates from the xyz file
!---------------------------------------
      fnamexyz= paste(sysname,'.xyz')

      length = lb(fnamein)

             write(11,*)
             write(11,*) 'Reading coordinates from file ',fnamexyz(1:length)


       open( unit=2, file=fnamexyz, form="formatted", status='old' )
       read(2,*) natoms
       do i=1,natoms
        read(2,*) label(i),(xat(i,ix),ix=1,3)
       enddo
        rat(1)=xat(choosatom,1)*Ang       
        rat(2)=xat(choosatom,2)*Ang       
        rat(3)=xat(choosatom,3)*Ang         
        write(11,*)'================================================'
        write(11,*) 'The integral will be calculated at point'
        write(11,*), rat(1),rat(2),rat(3), "Bohr"
        write(11,*)'That corresponds to atom ',label(choosatom)
        write(11,*)'at position',choosatom
        write(11,*)'================================================='
        close(2)


!Reads the potential at grid points

      allocate(POT(0:Mesh(1)-1,0:Mesh(2)-1,0:Mesh(3)-1,2),STAT=ierr)
      IF(ierr.NE.0) THEN
       WRITE(*,*),"POTENTIAL: ALLOCATION REQUEST DENIED"
       STOP
      ENDIF
      POT = 0.d0
       do isp=1,nspp 
         ind1=0
          do iz=0,Mesh(3)-1
            do iy=0,Mesh(2)-1
               read(1) (POT(ix,iy,iz,isp),ix=0,mesh(1)-1)
            enddo
          enddo
        enddo
      close(1)

      fmin = minval(POT)
      fmax = maxval(POT)
    
!      write(*,*) "minval, maxval:", fmin, fmax


!Loop over grid points to calculate the value of the orbital
!at each point


      ind2=0
      integral=0._dp
      potential=0._dp

      volume=volcel(cell)
      write(0,*) "volume", volume
      dvol=volume/real(NewMesh(1))/real(NewMesh(2))/real(NewMesh(3))
      write(0,*) "dvol:", dvol

      startx=1.0/(real(NewMesh(1)))
      starty=1.0/(real(NewMesh(2)))
      startz=1.0/(real(NewMesh(3)))

      do ix=1,3
        dr(ix)=cellm(ix)/mesh(ix)
      enddo
      drmax = maxval(dr)

      write(6,*)'dr',dr,drmax

      call c2f(1,rat,cell,ratfrac)

      do iz=1,mesh(3)
      do iy=1,mesh(2)
      do ix=1,mesh(1)

        DO iix=1,3
          rpoint0(iix)=(((dble(ix)-1))/dble(mesh(1))) * cell(iix,1)+&
                      ((dble(iy)-1)/dble(mesh(2))) * cell(iix,2) +&
                      ((dble(iz)-1)/dble(mesh(3))) * cell(iix,3)
        ENDDO
         
! Calculate the coordinate of the grid point relative 
! to the atom center in the supercell

        do iax=-1,1
        do iay=-1,1
        do iaz=-1,1

          DO iix=1,3
            ratt(iix)=rat(iix)+&
            iax*cell(iix,1)+iay*cell(iix,2)+iaz*cell(iix,3)
          ENDDO
      
          DO iix=1,3
            rrel(iix) = rpoint0(iix)-ratt(iix)
          ENDDO

          dist = dsqrt(rrel(1)**2+rrel(2)**2+rrel(3)**2)

          IF (dist .le. rcut+drmax) THEN

! Loop over mesh subpoints
            do mspx=1,factorx
            do mspy=1,factory
            do mspz=1,factorz

              DO iix=1,3
                rpoint(iix)= rpoint0(iix)+&
                     ((dble(mspx)-1)/dble(newmesh(1))) * cell(iix,1)+&
                     ((dble(mspy)-1)/dble(newmesh(2))) * cell(iix,2) +&
                     ((dble(mspz)-1)/dble(newmesh(3))) * cell(iix,3)
              ENDDO

              DO iix=1,3
                rrel(iix) = rpoint(iix)-ratt(iix)
              ENDDO

              dist = dsqrt(rrel(1)**2+rrel(2)**2+rrel(3)**2)

              IF (dist .le. rcut) THEN
                IF (dist.le. tiny4) dist=tiny4
                CALL splint(ao,af,y2,nmax,dist,orbr)

!SIESTA subroutine: returns the real spherical harmonics times r**l

                CALL rlylm(lmax,rrel,allang,Grly)
                shift=lmax*lmax
                DO IND3=1,LMAX*2+1
                  orbang(IND3)=(allang(IND3+shift))/dist**(lmax+1)
                ENDDO
                 
                integral = integral +((orbr*orbang(deg))**2)*dvol
                  
                a=(ix-1)/dble(mesh(1))+(mspx-1)/dble(NewMesh(1))
                b=(iy-1)/dble(mesh(2))+(mspy-1)/dble(NewMesh(2))
                c=(iz-1)/dble(mesh(3))+(mspz-1)/dble(NewMesh(3))
               
                call evaluate(POT(:,:,:,nspin),a,b,c,val)
                potential=potential+val*((orbr*orbang(deg))**2)*dvol   
              ENDIF 

            enddo
            enddo
            enddo

           ENDIF


         enddo
         enddo
         enddo

       enddo
       enddo
       enddo


       return

end subroutine siespart

      subroutine evaluate(d,a,b,c,val)

      implicit none

      real, intent(in) ::  d(0:,0:,0:)
      real(dp),intent(in)::a,b,c
      real(dp) ::  xfrac(3)           ! Reduced coordinates of point
      real(dp), intent(out) :: val

      integer n(3), lo(3), hi(3)
      real(dp)    r(3), x(3), y(3)
      integer i, j, k
      real(dp) ::  nk
      xfrac(1)=a
      xfrac(2)=b
      xfrac(3)=c

      n(1) = size(d,dim=1)
      n(2) = size(d,dim=2)
      n(3) = size(d,dim=3)
!           Find the right 3D "grid cube" and the reduced coordinates
!           of the point in it. The double mod assures that negative
!           numbers are well treated (the idea is to bring the coordinates
!           to the [0,n(k)) interval)
! 

            do k = 1, 3
               nk = real(n(k))
               r(k) =  modulo(n(k)*xfrac(k),nk)
               lo(k) = int(r(k))
               hi(k) = mod ( lo(k)+1, n(k) )
               x(k) = r(k) - lo(k)
               y(k) = 1 - x(k)
          !     print *, "rk, lok, hik, xk, yk:", r(k), lo(k), hi(k), x(k), y(k)

            enddo
       

!      compute charge density by linear interpolation

            val     = d(lo(1),lo(2),lo(3)) * y(1) * y(2) * y(3) +   &
     &                d(lo(1),lo(2),hi(3)) * y(1) * y(2) * x(3) +   &
     &                d(lo(1),hi(2),lo(3)) * y(1) * x(2) * y(3) +   &
     &                d(lo(1),hi(2),hi(3)) * y(1) * x(2) * x(3) +   &
     &                d(hi(1),lo(2),lo(3)) * x(1) * y(2) * y(3) +   &
     &                d(hi(1),lo(2),hi(3)) * x(1) * y(2) * x(3) +   &
     &                d(hi(1),hi(2),lo(3)) * x(1) * x(2) * y(3) +   &
     &                d(hi(1),hi(2),hi(3)) * x(1) * x(2) * x(3)


            end subroutine evaluate
end module dosiesta
