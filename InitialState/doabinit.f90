module doabinit

    USE precision
    USE types
    USE spher_harm
    USE interpola
    IMPLICIT NONE

    CONTAINS
    
    subroutine abinitpart(deg,lmax,factorx,factory,factorz,choosedeg,chooseae,&
&      choosatom,sysname,integral,potential)
      
        INTEGER,PARAMETER::natmax = 1000

        INTEGER,intent(in)    ::deg,lmax
        INTEGER      :: isp, ix, iy, iz, i, ip, natoms,np,     &
     &                  mesh(3), nspin, id(3), iix, iiy, &
     &                  iiz, length, lb,iax,iay,iaz,     &
     &                  NewMesh(3),ind3,ind1,ind2,idist, &
     &                  indexv,nskip,nmax=0,ios1=0,shift,iv
        integer, parameter               :: max_l = 5
        integer, parameter               :: max_ilm = (max_l+1)*(max_l+1)
        INTEGER::ierr
        INTEGER,intent(in)::choosatom,factorx,factory,factorz

        CHARACTER(LEN=8),intent(in)::choosedeg,chooseae
        CHARACTER(LEN=3)::label(natmax)
        CHARACTER   :: sysname*70, programa*75,fnamein*75, fnameout(2)*75,     &
        &              fnamexyz*75, paste*74, task*5

        REAL(dp)        ::rcut,dist,fmin,fmax,a,b,c,dvol
        real, allocatable      ::POT(:,:,:)
        REAL(dp)               ::dr(3), rpoint(3),rrel(3),ratt(3),xat(natmax,3), &
        &                        rat(3)
        real(dp)::grly(3,max_ilm),allang(max_ilm),cellm(3),celang(3)        
        real(dp),allocatable      :: ao(:),af(:),y2(:)
        real(dp)                  :: y1,yn,orbr,val,startx,starty,startz
        real(dp)              ::orbang(max_ilm),volume
        real(dp), intent(out) ::integral,potential
        real(dp), parameter      :: tiny4=1.e-4_dp
        real,parameter           :: tiny8=1.e-8_dp
        REAL(dp), parameter::PI=3.14159265358979323846264338327950288419716939937510_dp
        REAL(dp), parameter :: Ang    = 1._dp / 0.529177_dp
        real(dp), external :: volcel

        external  paste, lb

        !VARIABLES ABINIT
        integer                          :: nsppol,fform,rdwr,unitfi,INDEX
        integer                          :: ngfft(3),II,IJ,IK
        real(dp)                         :: rprimd(3,3)
        type (hdr_type)                  ::hdr
        real(dp),allocatable             ::RHO(:,:)
        real(dp), parameter              :: tol6=1.e-6_dp
        real(dp), parameter              :: zero=1.e-8_dp
        DOUBLE PRECISION HARTREE, RYDBERG
        PARAMETER(HARTREE = 2.D0)
        PARAMETER(RYDBERG = 13.6058D0)
        !END VARIABLES ABINIT 

!-------------------------
!Opens the AE wavefuncions                                                     
!-------------------------
        ios1=0

        open(unit=19,file=chooseae,status='old',action='read',iostat=ios1)
        DO
         read(19,*,iostat=ios1)
         IF (ios1<0) EXIT
         nmax=nmax+1
        ENDDO
        rewind(19)
        allocate(ao(nmax),af(nmax),y2(nmax))

        do i=1,nmax
         read(19,*)ao(i),af(i)
        enddo
        do i = nmax,1,-1
          if( af(i) >= 0.000000001) then
          rcut=ao(i)
          exit
          end if
        enddo
        close(19)
!       print*, rcut
! calls a subroutine that computes the derivate of the first and last points necessary to use with 
! the cubic spline subroutine


             call derivative(nmax,ao,af,y1,yn)
             call spline(ao,af,nmax,y1,yn,y2)
!-------------------------
!Reads the Total Potential
!-------------------------
      fnamein = paste(sysname,'_POT')
      length = lb(fnamein)

             write(11,*)
             write(11,*) 'Reading grid data from file ',fnamein(1:length)


        open( unit=1,file=fnamein, form="unformatted", status='old')
        rdwr=5    !read the header without rewind
        unitfi=1  !unidad del ficheritò

        call hdr_io_int(fform,hdr,rdwr,unitfi,ngfft,nspin,rprimd)
        write(11,*) 'Cell Vectors[Bohr]'
        write(11,*) rprimd(1,1),rprimd(2,1),rprimd(3,1)
        write(11,*) rprimd(1,2),rprimd(2,2),rprimd(3,2)
        write(11,*) rprimd(1,3),rprimd(2,3),rprimd(3,3)

        write(11,*)
        write(11,*) 'Grid: ',ngfft(1),'x',ngfft(2),'x',ngfft(3)
        write(11,*)
        write(11,*) 'nspin = ',nspin
        write(11,*)

        NewMesh(1)=ngfft(1)*factorx
        NewMesh(2)=ngfft(2)*factory
        NewMesh(3)=ngfft(3)*factorz
!cell vector modules
      do iv = 1,3
        cellm(iv) = dot_product(rprimd(:,iv),rprimd(:,iv))
        cellm(iv) = sqrt(cellm(iv))
      enddo

      celang(1) = dot_product(rprimd(:,1),rprimd(:,2))
      celang(1) = acos(celang(1)/(cellm(1)*cellm(2)))*180._dp/pi
      celang(2) = dot_product(rprimd(:,1),rprimd(:,3))
      celang(2) = acos(celang(2)/(cellm(1)*cellm(3)))*180._dp/pi
      celang(3) = dot_product(rprimd(:,2),rprimd(:,3))
      celang(3) = acos(celang(3)/(cellm(2)*cellm(3)))*180._dp/pi

      write(6,'(/,a,3f12.6)')                    &
     & 'outcell: Cell vector modules (Ang)   :', &
     &          (cellm(iv)/Ang,iv=1,3)
      write(6,'(a,3f12.4)')                      &
     & 'outcell: Cell angles (23,13,12) (deg):', &
     &          (celang(iv),iv=3,1,-1)


        IF (factorx.EQ.1.AND.factory.EQ.1.AND.factorz.EQ.1) THEN

celda:     do ix=1,3
            dr(ix)=cellm(ix)/Newmesh(ix)
           enddo celda
           write(11,*) 'Volume element='
           write(11,*) dr(1),'x',dr(2),'x',dr(3)
           write(11,*)
           np = Newmesh(1) * Newmesh(2) * Newmesh(3)
           write(11,*)'Total Number of Grid Points=',np
        ELSE

celdaL:     do ix=1,3
             dr(ix)=cellm(ix)/Newmesh(ix)
            enddo celdaL
            write(11,*)
            write(11,*) 'Volume element='
            write(11,*) dr(1),'x',dr(2),'x',dr(3) 
            np =NewMesh(1)*NewMesh(2)*NewMesh(3)
            write(11,*)
            write(11,*) 'New Grid: ',Newmesh(1),'x',Newmesh(2),'x',Newmesh(3)
            write(11,*)
            write(11,*)'Total Number of Grid Points=',np
       ENDIF


        ALLOCATE( RHO(ngfft(1)*ngfft(2)*ngfft(3),2) )
        READ(1) (RHO(IP,1),IP=1,ngfft(1)*ngfft(2)*ngfft(3))
            DO IP = 1, ngfft(1)*ngfft(2)*ngfft(3)
            RHO(IP,1) = RHO(IP,1) * HARTREE
        ENDDO
      close(1)

      fmin = minval(RHO)
      fmax = maxval(RHO)
    
!      write(6,*) "minval, maxval:", fmin, fmax
!--------------------------------------
!Reads the coordinates from the xyz file
!---------------------------------------
      fnamexyz= paste(sysname,'.xyz')

      length = lb(fnamein)

             write(11,*)
             write(11,*) 'Reading coordinates from file ',fnamexyz(1:length)

       open( unit=2, file=fnamexyz, form="formatted", status='old' )
       read(2,*) natoms
       do i=1,natoms
        read(2,*) label(i),(xat(i,ix),ix=1,3)
       enddo
        rat(1)=xat(choosatom,1)/0.529177
        rat(2)=xat(choosatom,2)/0.529177
        rat(3)=xat(choosatom,3)/0.529177
        write(11,*)'================================================'
        write(11,*) 'The integral will be calculated at point'
        write(11,*), rat(1),rat(2),rat(3),"Bohr"
        write(11,*)'That corresponds to atom ',label(choosatom)
        write(11,*)'at position',choosatom
        write(11,*)'================================================='
        close(18)

!Different way of reading the potential in case we need to interpolate or not

inter:  IF (factorx.EQ.1.AND.factory.EQ.1.AND.factorz.EQ.1) THEN
      write(11,*) "Interpolation will not be performed, I suppose you already have a refined grid"
!Start the loop over grid points

      ind2=0
      integral=0._dp
      potential=0._dp

      volume=volcel(rprimd)
      write(11,*) "volume", volume
      dvol=volume/np
      write(11,*) "dvol:", dvol

M1:DO iz=1,Newmesh(3)
M2:DO iy=1,Newmesh(2)
M3:DO ix=1,Newmesh(1)
       ind2=ind2+1
       DO iix=1,3
          rpoint(iix)=(((dble(ix)-1))/dble(Newmesh(1))) * rprimd(iix,1)+&
                      ((dble(iy)-1)/dble(Newmesh(2))) * rprimd(iix,2) +&
                      ((dble(iz)-1)/dble(Newmesh(3))) * rprimd(iix,3)
       ENDDO

! Calculate the coordinate of the grid point relative 
! to the atom center in the supercell

SX:        DO iax=-3,3
SY:        DO iay=-3,3
SZ:        DO iaz=-3,3
              DO iix=1,3
              ratt(iix)=rat(iix)+&
              iax*rprimd(iix,1)+iay*rprimd(iix,2)+iaz*rprimd(iix,3)
              ENDDO

              DO iix=1,3
              rrel(iix) = rpoint(iix)-ratt(iix)
              ENDDO
              dist = dsqrt(rrel(1)**2+rrel(2)**2+rrel(3)**2)


rcorte:        IF (dist .le. rcut) THEN

                  IF (dist.le. tiny4) dist=tiny4
                 CALL splint(ao,af,y2,nmax,dist,orbr)

!SIESTA subroutine: returns the real spherical harmonics times r**l

                  CALL rlylm(lmax,rrel,allang,Grly)
                  shift=lmax*lmax
                  DO IND3=1,LMAX*2+1
                     orbang(IND3)=(allang(IND3+shift))/dist**(lmax+1)
                  ENDDO
                  integral = integral +((orbr*orbang(deg))**2)*dvol
                  potential=potential+RHO(ind2,1)*((orbr*orbang(deg))**2)*dvol
               ENDIF rcorte
           ENDDO SZ
           ENDDO SY
           ENDDO SX
  ENDDO M3
  ENDDO M2
  ENDDO M1
     
!If we have to interpolate ....   
     ELSE
     
     write(11,*) "Interpolation will be performed"
 

!Reads the potential at grid points

    allocate(POT(0:ngfft(1)-1,0:ngfft(2)-1,0:ngfft(3)-1))
      POT = 0.d0
          if (nspin > 1) write(0,*)  "** Only 1st spin info can be read at this point"
         ind1=0
        DO IK=0,ngfft(3)-1
           DO IJ=0,ngfft(2)-1
               DO II=0,ngfft(1)-1
                  INDEX=ngfft(2)*ngfft(1)*(IK)+ngfft(1)*(IJ)+II+1
                  POT(II,IJ,IK)=RHO(INDEX,1)
        ENDDO
           ENDDO
               ENDDO
      fmin = minval(POT)
      fmax = maxval(POT)

!      write(0,*) "minval, maxval:", fmin, fmax

      deallocate (RHO)

      ind2=0
      integral=0._dp
      potential=0._dp

      volume=volcel(rprimd)
      write(0,*) "volume", volume
      dvol=volume/np
      write(0,*) "dvol:", dvol

      startx=1.0/(real(NewMesh(1)))
      starty=1.0/(real(NewMesh(2)))
      startz=1.0/(real(NewMesh(3)))

M1A:DO iz=1,Newmesh(3)
M2A:DO iy=1,Newmesh(2)
M3A:DO ix=1,Newmesh(1)
       ind2=ind2+1
       DO iix=1,3
          rpoint(iix)=(((dble(ix)-1))/dble(Newmesh(1))) * rprimd(iix,1)+&
                      ((dble(iy)-1)/dble(Newmesh(2))) * rprimd(iix,2) +&
                      ((dble(iz)-1)/dble(Newmesh(3))) * rprimd(iix,3)
       ENDDO
! Calculate the coordinate of the grid point relative 
! to the atom center in the supercell

SXA:        DO iax=-3,3
SYA:        DO iay=-3,3
SZA:        DO iaz=-3,3
              DO iix=1,3
              ratt(iix)=rat(iix)+&
              iax*rprimd(iix,1)+iay*rprimd(iix,2)+iaz*rprimd(iix,3)
              ENDDO

              DO iix=1,3
              rrel(iix) = rpoint(iix)-ratt(iix)
              ENDDO
              dist = dsqrt(rrel(1)**2+rrel(2)**2+rrel(3)**2)


rcorteA:        IF (dist .le. rcut) THEN

                  IF (dist.le. tiny4) dist=tiny4
                 CALL splint(ao,af,y2,nmax,dist,orbr)

!SIESTA subroutine: returns the real spherical harmonics times r**l

                  CALL rlylm(lmax,rrel,allang,Grly)
                  shift=lmax*lmax
                  DO IND3=1,LMAX*2+1
                     orbang(IND3)=(allang(IND3+shift))/dist**(lmax+1)
                  ENDDO

                    integral = integral +((orbr*orbang(deg))**2)*dvol
                    a=(ix-1)*startx
                    b=(iy-1)*starty
                    c=(iz-1)*startz
                    call evaluate(POT(:,:,:),a,b,c,val)
                    potential=potential+val*((orbr*orbang(deg))**2)*dvol
               ENDIF rcorteA
           ENDDO SZA
           ENDDO SYA
           ENDDO SXA
  ENDDO M3A
  ENDDO M2A
  ENDDO M1A

         ENDIF inter

     CONTAINS

           subroutine hdr_io_int(fform,hdr,rdwr,unitfi,ngfft,nsppol,rprimd)

!Arguments ------------------------------------
 integer,intent(inout) :: fform
 integer,intent(in) :: rdwr,unitfi
 type(hdr_type),intent(inout) :: hdr
 integer::ngfft(1:3)
 real(dp)::rprimd(3,3)

!Local variables-------------------------------
 integer :: bantot,bsize,cplex,headform,iatom,ierr,ii,ikpt,ipsp,ispden,isym
 integer :: jj,lloc,lmax,mmax,natinc,natom,nkpt,npsp,nselect,nsppol
 integer :: nsym,ntypat
 character(len=500) :: message
 character(len=6) :: codvsn
 integer, allocatable :: ibuffer(:),nsel44(:,:),nsel56(:)
 real(dp) :: acell(3)
 real(dp), allocatable :: buffer(:)
 real(dp), parameter              :: zero=1.e-8_dp
! *************************************************************************

!DEBUG
!write(6,*)' hdr_io : enter hdr_io_int '
 call flush(6)
!ENDDEBUG

! -------------------------------------------------------------------------
! Reading the header of an unformatted file
! -------------------------------------------------------------------------

  if (rdwr==1) then
   rewind(unitfi)
  end if

! Reading the first record of the file ------------------------------------

  read(unitfi,iostat=ierr)codvsn,fform
  if (ierr /=0) then
   fform=0
   return   ! This is to allow treatment of old epsm1 format
  end if
!  Format beyond 22 have a different first line, so need reading again the first line
   backspace (unitfi)
   read (unitfi)   codvsn,headform,fform
  hdr%codvsn=codvsn
  hdr%headform=headform
! fform is not a record of hdr_type

!DEBUG
! write(6,*)' hdr_io : debug '
! write(6,*)' hdr_io : codvsn,headform,fform',codvsn,headform,fform
!ENDDEBUG

! Reading the second record of the file ------------------------------------

! Initialize the values that are not present for all versions (exception : npsp)
  hdr%nspden=1
  hdr%nspinor=1
  hdr%occopt=1
  hdr%pertcase=1
  hdr%usepaw=0
! XG070408 : note that the location for the usewvl variable in the actually written/read header is not yet defined
! However, the hdr%usewvl quantity is already tested in hdr_check.F90 ...

  hdr%usewvl=0
  hdr%ecut=zero
  hdr%ecutdg=zero
  hdr%ecutsm=zero
  hdr%qptn(1:3)=zero
  hdr%stmbias=zero
  hdr%tphysel=zero
  hdr%tsmear=zero
!   Compared to v4.2, add usepaw and ecutdg
    read(unitfi) bantot, hdr%date, hdr%intxc, hdr%ixc, natom, hdr%ngfft(1:3),&
&    nkpt, hdr%nspden, hdr%nspinor, nsppol, nsym, npsp, ntypat, hdr%occopt, hdr%pertcase,&
&    hdr%usepaw, hdr%ecut, hdr%ecutdg, hdr%ecutsm, hdr%ecut_eff, hdr%qptn(1:3), hdr%rprimd,&
&    hdr%stmbias, hdr%tphysel, hdr%tsmear

!    print*,"bantot, hdr%date, hdr%intxc, hdr%ixc, natom, hdr%ngfft(1:3),&
!&    nkpt, hdr%nspden, hdr%nspinor, nsppol, nsym, npsp, ntypat, hdr%occopt, hdr%pertcase,&
!&    hdr%usepaw, hdr%ecut, hdr%ecutdg, hdr%ecutsm, hdr%ecut_eff, hdr%qptn(1:3), hdr%rprimd,&
!&    hdr%stmbias, hdr%tphysel, hdr%tsmear",bantot, hdr%date, hdr%intxc, hdr%ixc, natom, hdr%ngfft(1:3),&
!&    nkpt, hdr%nspden, hdr%nspinor, nsppol, nsym, npsp, ntypat, hdr%occopt, hdr%pertcase,&
!&    hdr%usepaw, hdr%ecut, hdr%ecutdg, hdr%ecutsm, hdr%ecut_eff, hdr%qptn(1:3), hdr%rprimd,&
!&    hdr%stmbias, hdr%tphysel, hdr%tsmear

  hdr%bantot=bantot
  hdr%natom =natom
  hdr%nkpt  =nkpt
  hdr%npsp  =npsp
  hdr%nsppol=nsppol
  hdr%nsym  =nsym
  hdr%ntypat =ntypat
  ngfft(1)=hdr%ngfft(1)
  ngfft(2)=hdr%ngfft(2)
  ngfft(3)=hdr%ngfft(3)
  rprimd(1,1)=hdr%rprimd(1,1)
  rprimd(1,2)=hdr%rprimd(1,2)
  rprimd(1,3)=hdr%rprimd(1,3)
  rprimd(2,1)=hdr%rprimd(2,1)
  rprimd(2,2)=hdr%rprimd(2,2)
  rprimd(2,3)=hdr%rprimd(2,3)
  rprimd(3,1)=hdr%rprimd(3,1)
  rprimd(3,2)=hdr%rprimd(3,2)
  rprimd(3,3)=hdr%rprimd(3,3)
!DEBUG
!  write(6,*)' hdr_io : before allocate '
!  write(6,*)' hdr_io : bantot,natom,nkpt,npsp,nsppol,nsym,ntypat',&
!&  bantot,natom,nkpt,npsp,nsppol,nsym,ntypat,rprimd
!ENDDEBUG

! Allocate all parts of hdr that need to be --------------------------------

  allocate(hdr%istwfk(nkpt))
  allocate(hdr%kptns(3,nkpt))
  allocate(hdr%lmn_size(npsp))
  allocate(hdr%nband(nkpt*nsppol))
  allocate(hdr%npwarr(nkpt)) ! Warning : npwarr here has only one dim
  allocate(hdr%occ(bantot))
  allocate(hdr%pspcod(npsp))
  allocate(hdr%pspdat(npsp))
  allocate(hdr%pspso(npsp))
  allocate(hdr%pspxc(npsp))
  allocate(hdr%so_psp(npsp))
  allocate(hdr%symafm(nsym))
  allocate(hdr%symrel(3,3,nsym))
  allocate(hdr%title(npsp))
  allocate(hdr%tnons(3,nsym))
  allocate(hdr%typat(natom))
  allocate(hdr%wtk(nkpt))
  allocate(hdr%xred(3,natom))
  allocate(hdr%zionpsp(npsp))
  allocate(hdr%znuclpsp(npsp))
  allocate(hdr%znucltypat(ntypat))

!DEBUG
! write(6,*)' hdr_io : after allocate '
!ENDDEBUG

! Reading the third record of the file ------------------------------------

! Initialize the values that are not present for all versions
  hdr%istwfk(:)=1
  hdr%so_psp(:)=1
  hdr%symafm(:)=1

!   Compared to pre v5.0, add wtk
    read(unitfi)  hdr%istwfk(:), hdr%nband(:), hdr%npwarr(:), &
&    hdr%so_psp(:), hdr%symafm(:), hdr%symrel(:,:,:), &
&    hdr%typat(:), hdr%kptns(:,:), hdr%occ(:), &
&    hdr%tnons(:,:), hdr%znucltypat(:), hdr%wtk(:)

! Reading the records with psp information ---------------------------------

! Initialize the values that are not present for all versions
  hdr%pspso(:)=1
  hdr%lmn_size(:)=0

   do ipsp=1,npsp
    read(unitfi) hdr%title(ipsp), hdr%znuclpsp(ipsp), &
&    hdr%zionpsp(ipsp), hdr%pspso(ipsp), hdr%pspdat(ipsp), &
&    hdr%pspcod(ipsp), hdr%pspxc(ipsp), hdr%lmn_size(ipsp)
   end do

! Reading the final record of the header  ----------------------------! Initialize the values that are not present for all versions
  hdr%fermie=zero

  if(headform==22)then
  read(unitfi) hdr%residm, hdr%xred(:,:), hdr%etot
  else if(headform==23 .or. headform==34 .or. headform>=40)then
   read(unitfi) hdr%residm, hdr%xred(:,:), hdr%etot, hdr%fermie
  end if

!DEBUG
!  write(6,*)' hdr_io : read mode, hdr%so_psp(:), hdr%symafm(:)=',&
!&                                 hdr%so_psp(:), hdr%symafm(:)
!ENDDEBUG

end subroutine hdr_io_int
!!***
      subroutine evaluate(d,a,b,c,val)

      implicit none

      real, intent(in) ::  d(0:,0:,0:)
      real(dp),intent(in)::a,b,c
      real(dp) ::  xfrac(3)           ! Reduced coordinates of point
      real(dp), intent(out) :: val

      integer n(3), lo(3), hi(3)
      real(dp)    r(3), x(3), y(3)
      integer i, j, k
      real(dp) ::  nk
      xfrac(1)=a
      xfrac(2)=b
      xfrac(3)=c

      n(1) = size(d,dim=1)
      n(2) = size(d,dim=2)
      n(3) = size(d,dim=3)
!           Find the right 3D "grid cube" and the reduced coordinates
!           of the point in it. The double mod assures that negative
!           numbers are well treated (the idea is to bring the coordinates
!           to the [0,n(k)) interval)
! 

            do k = 1, 3
               nk = real(n(k))
               r(k) =  modulo(n(k)*xfrac(k),nk)
               lo(k) = int(r(k))
               hi(k) = mod ( lo(k)+1, n(k) )
               x(k) = r(k) - lo(k)
               y(k) = 1 - x(k)
          !     print *, "rk, lok, hik, xk, yk:", r(k), lo(k), hi(k), x(k), y(k)

            enddo
       

!      compute charge density by linear interpolation

            val     = d(lo(1),lo(2),lo(3)) * y(1) * y(2) * y(3) +   &
     &                d(lo(1),lo(2),hi(3)) * y(1) * y(2) * x(3) +   &
     &                d(lo(1),hi(2),lo(3)) * y(1) * x(2) * y(3) +   &
     &                d(lo(1),hi(2),hi(3)) * y(1) * x(2) * x(3) +   &
     &                d(hi(1),lo(2),lo(3)) * x(1) * y(2) * y(3) +   &
     &                d(hi(1),lo(2),hi(3)) * x(1) * y(2) * x(3) +   &
     &                d(hi(1),hi(2),lo(3)) * x(1) * x(2) * y(3) +   &
     &                d(hi(1),hi(2),hi(3)) * x(1) * x(2) * x(3)


            end subroutine evaluate
     end subroutine abinitpart
END MODULE doabinit
